module register (
	input logic         i_clk,
	input logic         i_rst,
	input logic         i_en,
	input logic  [15:0] i_d,
	output logic [15:0] o_q
);
	
	logic [15:0] r;

	always @(posedge i_clk, posedge i_rst)
	begin
		if (i_rst) begin
			r <= 0;
		end else if (i_en) begin
			r <= i_d;
		end
	end

	assign o_q = r;

endmodule
