`include "uvm_macros.svh"

package register_test_pkg;
	import uvm_pkg::*;

	class SequenceItem extends uvm_sequence_item;
		typedef enum {
			DRIVER,
			MONITOR
		} origin_t;
		typedef enum {
			RESET,
			DATA
		} type_t;

		origin_t          m_origin;
		type_t            m_type;
		rand int unsigned m_delay;
		rand logic [15:0] m_data;

		constraint c_delay { m_delay inside {[0:10]}; }

		`uvm_object_utils_begin(SequenceItem)
			`uvm_field_enum (origin_t, m_origin, UVM_DEFAULT)
			`uvm_field_enum (type_t,   m_type,   UVM_DEFAULT)
			`uvm_field_int  (          m_delay,  UVM_DEFAULT)
			`uvm_field_int  (          m_data,   UVM_DEFAULT)
		`uvm_object_utils_end

		function new (string name = "SequenceItem");
			super.new(name);
		endfunction

	endclass

	class Sequence extends uvm_sequence;
		`uvm_object_utils(Sequence)

		function new (string name = "Sequence");
			super.new(name);
		endfunction

		virtual task body();
			for (int unsigned i = 0; i < 10; ++i)
			begin
				SequenceItem item = SequenceItem::type_id::create();
				item.m_type = SequenceItem::DATA;
				start_item(item);
				item.randomize();
				finish_item(item);
			end
		endtask
	endclass

	class Driver extends uvm_driver #(SequenceItem);
		`uvm_component_utils (Driver)

		uvm_analysis_port #(SequenceItem) drv_port;
		virtual register_if m_if;

		function new (
			string name = "Driver",
			uvm_component parent = null
		);
			super.new(name, parent);
		endfunction

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);

			if (!uvm_config_db#(virtual register_if)::get(this, "", "dut_if", m_if)) begin
				`uvm_fatal("DRIVER", "Did not get interface handle")
			end
			
			drv_port = new("drv_port", this);
		endfunction

		virtual task run_phase (uvm_phase phase);
			super.run_phase(phase);
			forever begin
				SequenceItem item;
				seq_item_port.get_next_item(item);
				drive_item(item);
				item.m_origin = SequenceItem::DRIVER;
				drv_port.write(item);
				fork
				seq_item_port.item_done();
				begin
					phase.raise_objection(this);
					repeat (2) @(m_if.cb);
					phase.drop_objection(this);
				end
				join_any
			end
		endtask

		virtual task drive_item (SequenceItem item);
			case (item.m_type)

			SequenceItem::RESET: begin
				m_if.rst <= 1;
				repeat (item.m_delay) @(m_if.cb);
				m_if.cb.rst <= 0;
			end
			SequenceItem::DATA: begin
				$display("[DRV] delay=%d d=%x", item.m_delay, item.m_data);

				repeat (item.m_delay)
				begin
					@(m_if.cb);
					m_if.cb.en <= 0;
				end

				if (!m_if.rst) begin
					m_if.cb.en <= 1;
					m_if.cb.d  <= item.m_data;
				end
				 @(m_if.cb);
				 m_if.cb.en <= 0;
			end
			endcase

		endtask
	endclass

	class Monitor extends uvm_monitor;
		`uvm_component_utils (Monitor)

		uvm_analysis_port #(SequenceItem) mon_port;
		virtual register_if m_if;

		function new (
			string name = "Monitor",
			uvm_component parent = null
		);
			super.new(name, parent);
		endfunction

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);

			if (!uvm_config_db#(virtual register_if)::get(this, "", "dut_if", m_if)) begin
				`uvm_fatal("MONITOR", "Did not get interface handle")
			end

			mon_port = new ("mon_port", this);
		endfunction

		virtual task run_phase (uvm_phase phase);
			super.run_phase(phase);
			forever begin
				SequenceItem item = new();
				item.m_type = SequenceItem::DATA;
				item.m_origin = SequenceItem::MONITOR;

				@(m_if.cb);
				if (m_if.en) begin
					fork
					begin
						@(m_if.cb);
						item.m_data = m_if.cb.q;

						mon_port.write(item);

						$display("[MON] d=%x", item.m_data);
					end
					join_none
				end
			end
		endtask

	endclass

	class Agent extends uvm_agent;
		`uvm_component_utils(Agent)

		function new (string name = "Agent", uvm_component parent = null);
			super.new(name, parent);
		endfunction

		Driver                        d0;
		Monitor                       m0;
		uvm_sequencer #(SequenceItem) s0;

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);

			d0 = Driver::type_id::create("d0", this);
			m0 = Monitor::type_id::create("m0", this);
			s0 = uvm_sequencer#(SequenceItem)::type_id::create("s0", this);
		endfunction

		virtual function void connect_phase (uvm_phase phase);
			super.connect_phase(phase);
			d0.seq_item_port.connect(s0.seq_item_export);
		endfunction
	endclass

	class Scoreboard extends uvm_scoreboard;
		`uvm_component_utils(Scoreboard)

		function new (string name = "Scoreboard", uvm_component parent = null);
			super.new(name, parent);
		endfunction

		SequenceItem m_ref_seq [$];

		uvm_analysis_imp #(SequenceItem, Scoreboard) drv_port;
		uvm_analysis_imp #(SequenceItem, Scoreboard) mon_port;

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);
			drv_port = new("drv_port", this);
			mon_port = new("mon_port", this);
		endfunction

		virtual function write (SequenceItem item);
			string s = "";

			case (item.m_origin)
			SequenceItem::DRIVER: begin
				m_ref_seq.push_back(item.clone());
				$sformat(s, "orig=DRIVER");
			end
			SequenceItem::MONITOR: $sformat(s, "orig=MONITOR");
			endcase

			$sformat(s, "%s d=%x", s, item.m_data);
			`uvm_info("SCB", s, UVM_LOW);
		endfunction
	endclass

	class Environment extends uvm_env;
		`uvm_component_utils(Environment)

		function new (string name = "Environment", uvm_component parent = null);
			super.new(name, parent);
		endfunction

		Agent      a0;
		Scoreboard s0;

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);

			a0 = Agent::type_id::create("a0", this);
			s0 = Scoreboard::type_id::create("s0", this);
		endfunction

		virtual function void connect_phase (uvm_phase phase);
			super.connect_phase(phase);
			a0.d0.drv_port.connect(s0.drv_port);
			a0.m0.mon_port.connect(s0.mon_port);
		endfunction
		
	endclass

	class Test extends uvm_test;
		`uvm_component_utils(Test)

		virtual register_if m_if;

		function new (string name = "Test", uvm_component parent = null);
			super.new(name, parent);
		endfunction

		Environment e0;

		virtual function void build_phase (uvm_phase phase);
			super.build_phase(phase);

			e0 = Environment::type_id::create("e0", this);

			if (!uvm_config_db#(virtual register_if)::get(this, "", "dut_if", m_if)) begin
				`uvm_fatal("DRIVER", "Did not get interface handle")
			end
		endfunction

		virtual task run_phase (uvm_phase phase);
			Sequence seq = Sequence::type_id::create();
			phase.raise_objection(this);
			apply_reset();
			seq.start(e0.a0.s0);
			phase.drop_objection(this);
		endtask

		virtual task apply_reset();
			m_if.rst <= 1;
			@(m_if.cb);
			m_if.rst <= 0;
		endtask;
		
	endclass

endpackage
