interface register_if (
	input logic clk
);

	logic        rst;
	logic        en;
	logic [15:0] d;
	logic [15:0] q;

	
	clocking cb @(posedge clk);
		output rst;
		output en;
		output d;
		input  q;
	endclocking

endinterface
