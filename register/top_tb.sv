module top_tb;
	import uvm_pkg::*;
	import register_test_pkg::*;

	logic w_clk;

	register_if _if (w_clk);

	register dut (
		.i_clk(_if.clk),
		.i_rst(_if.rst),
		.i_en(_if.en),
		.i_d(_if.d),
		.o_q(_if.q)
	);

	/*********************************************************************/

	initial
	begin
		uvm_config_db#(virtual register_if)::set(null, "", "dut_if", _if);

		run_test("Test");
	end

	initial
	begin : p_clock
		w_clk <= 0;
		forever
		begin
			#100 w_clk <= ~w_clk;
		end
	end : p_clock
endmodule
